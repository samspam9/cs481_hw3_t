﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class Mail
    {
        public string Sender { get; set; }
        public string Object { get; set; }
        public string MailContent { get; set; }
    }
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
            var Mails = new List<Mail>();
            Mails.Add(new Mail { Sender = "Michael Reeves", Object = "New robot idea", MailContent = "Hey, I have this new idea I wa..." });
            Mails.Add(new Mail { Sender = "Damso", Object = "Beats", MailContent = "Here are the beat samples I made, choose one." });
            Mails.Add(new Mail { Sender = "Barack Obama", Object = "Request", MailContent = "Please run fro president." });
            Mails.Add(new Mail { Sender = "Chinese Corp", Object = "Congratulations!!", MailContent = "Awesome!!1 You're the 108977th customer to..." });
            Mails.Add(new Mail { Sender = "RDJ", Object = "New movie", MailContent = "Hello, Mind replacing me in my new movie?" });
            Mails.Add(new Mail { Sender = "Nairo", Object = "Tips", MailContent = "Got any tips to improve my play?" });
            Mails.Add(new Mail { Sender = "Usain Bolt", Object = "Too slow", MailContent = "How are you so fast??" });
            Mails.Add(new Mail { Sender = "Papa Jhon's", Object = "I lied", MailContent = "I didn't eat 40 pizzas in 2 weeks, don't tell anyone..." });
            listView.ItemsSource = Mails;
        }
    }
}