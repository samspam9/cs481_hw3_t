﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class News
    {
        public string Org { get; set; }
        public string Title { get; set; }
    }
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
            var NewsList = new List<News>();
            NewsList.Add(new News { Org = "NY Times", Title = "How do we deal with the Corona Virus?" });
            NewsList.Add(new News { Org = "LA Times", Title = "What's Corona Virus?" });
            NewsList.Add(new News { Org = "IGN", Title = "MKLeo is invincible" });
            NewsList.Add(new News { Org = "Kitchen Times", Title = "Best knife in the market" });
            NewsList.Add(new News { Org = "NBCNews", Title = "What did trump do today?" });
            NewsList.Add(new News { Org = "Fox News", Title = "Let's talk about foxes" });
            NewsList.Add(new News { Org = "Yahoo", Title = "Do people even read this?" });
            NewsList.Add(new News { Org = "VolleyBall times", Title = "Why is volleyall the best sport?" });

            listView.ItemsSource = NewsList;
        }
    }
}